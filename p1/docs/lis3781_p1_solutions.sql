-- Begin Reports --

-- 1)Create a view that displays attorneys’ *full* names, *full* addresses, ages, hourly rates, the bar names that they’ve passed,
-- as well as their specialties, sort by attorneys’ last names.

drop view if exists v_attorney_info;
create view v_attorney_info as
	select
    concat(per_lname, ", ", per_fname) as name,
    concat(per_street, ", ", per_city, ", ", per_state, " ", per_zip) as address,
    TIMESTAMPDIFF(year, per_dob, now()) as age,
    concat('$', FORMAT(aty_hourly_rate,2)) as hourly_rate,
    bar_name, spc_type
    from person
		natural join attorney
        natural join bar
        natural join specialty
        order by per_lname;
	select * from v_attorney_info;
    drop VIEW if exists v_attorney_info;

-- 2)Create a stored procedurethat displayshow many judgeswere born in each month of the year, sorted by month.

drop procedure if exists sp_num_judges_born_by_month;
DELIMITER //
create procedure sp_num_judges_born_by_month()
BEGIN
	select month(per_dob) as month, monthname(per_dob) as month_name, count(*) as count
    from person
    natural join judge
    group by month_name
    order by month;
END //
DELIMITER ;

call sp_num_judges_born_by_month();
drop procedure if exists sp_num_judges_born_by_month;

-- 3)Create a stored procedure that displays *all* case types and descriptions, as well as judges’ *full* names,
-- *full* addresses, phone numbers, years in practice, for cases that they presided over, with their start and end dates, sort by judges’ last names.

drop procedure if exists sp_cases_and_judges;
DELIMITER //
create procedure sp_cases_and_judges()
begin
	select per_id, cse_id, cse_type, cse_description,
		concat(per_fname, " ", per_lname) as name,
        concat('(',substring(phn_num, 1, 3), ')', substring(phn_num, 4, 3) '-', substring(phn_num, 7, 4)) as judge_phone_num,
        phn_type,
        jud_years_in_practice,
        cse_start_date,
        cse_end_date
	from person
		natural join judge
        natural join `case`
        natural join phone
	where per_type='j'
    order by per_lname;
    
END //
DELIMITER ;
call sp_cases_and_judges();
drop procedure if exists sp_cases_and_judges;
-- 4) Create a trigger that automatically adds a record to the judge history table for every record added to the judge table.
/*
select '4) Create a trigger that automatically adds a record to the judge history table for every record **added** to the judge table.' as "";
-- do sleep(5);

select 'show person data *before* adding person record' as "";
select per_id, per_fname, per_lname from person;
-- do sleep(5);

set @salt=random_bytes(64);
set @num=000000000;
set @ssn=unhex(sha2(concat(@salt, @num),512));

insert into person
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
values
(null, @ssn, @salt, 'Bobby', 'Sue', '123 Main St', 'Panama City Beach', 'FL', 324530221, 'bsue@fl.gov', '1962-05-16', 'j', 'new district judge');

select 'show person data *after* adding person record' as "";
select per_id, per_fname, per_lname from person;
-- do sleep(5);

select 'show judge/judge_hist data *before* AFTER INSERT trigger fires (trg_judge_history_after_insert)' as "";
select * from judge;
select * from judge_hist;
-- do sleep(7);
*/
DROP TRIGGER IF EXISTS trg_judge_history_insert;
DELIMITER //
CREATE TRIGGER trg_judge_history_insert
AFTER INSERT ON judge
FOR EACH ROW
BEGIN
	insert into judge_hist
    (per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
    values
    (
    new.per_id, new.crt_id, current_timestamp(), 'i', new.jud_salary,
    concat("modifying user: ", user(), "Notes: ", new.jud_notes)
    );
END //
DELIMITER ;
drop trigger if exists trg_judge_history_insert;
/*select 'fire trigger by inserting record into judge table' as "";
-- do sleep(5);

insert into judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
values
((select count(per_id) from person), 3, 175000, 31, 'transferred from neighboring jurisdiction');

select 'show judge/judge_hist data *after* AFTER INSERT trigger fires *trg_judge_history_after_insert)' as '';
select * from judge;
select * from judge_hist;
#do sleep(7);*/


-- 5 Create a trigger that automatically adds a record to the judge history table for every record modified in the judge table.

DROP TRIGGER IF EXISTS trg_judge_history_update;
DELIMITER //
CREATE TRIGGER trg_judge_history_update
AFTER UPDATE ON judge
FOR EACH ROW
begin
	insert into judge_hist
    (per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
    values
    (
    NEW.per_id, new.crt_id, current_timestamp(), 'u', new.jud_salary,
    concat("modifying user: ", user(), " Notes: ", new.jud_notes)
    );
END //
DELIMITER ;
drop trigger if exists trg_judge_history_update;    
    
-- 6) Create a one-time event that executes one hour following its creation,
-- the event should add a judge record (one more than the required five records), 
-- have the event call a stored procedure that adds the record (name it one_time_add_judge).

drop event if exists one_time_add_judge;
DELIMITER //
create event if not exists one_time_add_judge
on schedule
at now() + interval 1 hour
comment 'adds a judge record only one-time'
do
begin
	call sp_add_judge_record();
end//

DELIMITER ;
drop event if exists one_time_add_judge;

create procedure sp_add_judge_record()
begin
	insert into judge
    (per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
    values
    (6, 1, 110000, 0, concat("New judge was former attorney. ", "Modifying event creator: ", current_user()))
end //

DELIMITER ;


-- EC

drop event if exists remove_judge_history;
DELIMITER //
create event if not exists remove_judge_history
on schedule
	every 2 month
starts now() + interval 3 week
ends now() + interval 4 year
comment 'keeps only the first 100 judge records'
do
begin
	delete from judge_hist where jhs_id > 100;
end//

DELIMITER ;
drop event if exists remove_judge_history;
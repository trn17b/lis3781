# LIS3781 - Advance Database Management

## Troy Noble

### Project #1 Requirements:

1. Create the database based on local municipality requirements
2. Create SQL statements to complete required tasks
3. Create ERD and forward engineer data to CCI server

#### README.md file should include the following items:

* Screenshot of ERD
* SQL code for the required reports

#### Assignment Screenshots:

*Screenshot of completed ERD*:

![Completed ERD](img/p1_erd.png "ERD for P1")

SQL code:

[P1 SQL](docs/lis3781_p1_solutions.sql)


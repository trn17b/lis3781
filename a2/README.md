# LIS3781 - Advanced Database Management

## Troy Noble

### Assignment 2 Requirements:

1. Screenshots of SQL code
2. Screenshots of populated tables
3. Bitbucket repo link
4. Granting permissions
5. Questions

#### README.md file should include the following items:

* Screenshots of lis3781_a2_solutions.sql
* Screenshot of populated customer and company table

a. Use 1:M relationship: **company** is <u>parent</u> table

b. **Company** attributes: 

​	i.     cmp_id (pk)

​	ii.	cmp_type enum ('C-Corp','S-Corp','Non-Profit-Corp','LLC','Partnership')

​	iii.   cmp_street

​	iv.   cmp_city

​	v.	cmp_state

​	vi.   cmp_zip (zf)

​	vii.  cmp_phone

​	viii. cmp_ytd_sales

​	ix.   cmp_url

​	x.    cmp_notes

c. **Customer** attributes:

​	i.	cus_id(pk)

​	ii.   cmp_id(fk)

​	iii.  cus_ssn(binary64

​	iv.  cus_salt (binary64)

​	v.   cus_type enum('Loyal','Discount','Impulse','Need-Based','Wandering')

​	vi.  cus_first

​	vii. cus_last

​	viii.cus_street

​	ix.  cus_city

​	x.   cus_state

​	xi.  cus_zip(zf)

​	xii. cus_phone

​	xiii.cus_email

​	xiv.cus_balance

​	xv. cus_tot_sales

​	xvi.cus_notes

d. Create suitable indexes and foreign keys:

​		Enforce pk/fk relationship: on update cascade, on delete restrict



#### Assignment Screenshots:

*Screenshot of lis3781_a2_code_a*:

![First part of SQL code](img/lis3781_a2_code_a.png "Code part A")

*Screenshot of lis3781_a2_code_b*:

![Second part of SQL code](img/lis3781_a2_code_b.png "Second part of code")

*Screenshot of SQL Table results*:

![Tables with populated results](img/a2_code_results.png "Populated tables")


# LIS3781 - Advanced Database Management

## Troy Noble

### Project #2 Requirements:

#### README.md file should include the following items:

* Screenshot of mongoDB running and functional
* Show JSON code for reports

#### Assignment Screenshot:

*Screenshot of mongoDB running*

![mongoDB running](img/mongo_running.png "Mongo Running")

#### JSON Code

```
1. db.restaurants.find()
2. db.restaurants.find().count()
3. db.restaurants.find().limit(5)
4. db.restaurants.find( { "borough": "Brooklyn" } )
5. db.restaurants.find( { "cuisine": "American " } )
6. db.restaurants.find( { "borough": "Manhattan", "cuisine": "Hamburgers" } )
7. db.restaurants.find( { "borough": "Manhattan", "cuisine": "Hamburgers" } ).count()
8. db.restaurants.find( {"address.zipcode": "10075"} )
9. db.restaurants.find( {"address.zipcode": "10075", "cuisine": "Chicken"} )
10. db.restaurants.find({ $or: [{ "cuisine": "Chicken" } , { "address.zipcode": "10024" } ] })
11. db.restaurants.find( {"borough": "Queens", "cuisine": "Jewish/Kosher"} ).sort({"address.zipcode": -1})
12. db.restaurants.find( { "grades.grade": "A" } )
13. db.restaurants.find( { "grades.grade": "A" } , { "name": 1, "grades.grade": 1 } )
14. db.restaurants.find( { "grades.grade": "A" } , { "name": 1, "grades.grade": 1, _id:0 } )
15. db.restaurants.find( { "grades.grade": "A" }).sort({ "cuisine": 1, "address.zipcode": -1 } )
16. db.restaurants.find( { "grades.score": { $gt:80} } )
17. db.restaurants.insert({"address":{
    "street" : "7th Avenue",
    "zipcode" : "10024",
    "building" : "1000",
    "coord" : [-58.9557413, 31.7720226 ]
    },
    "borough" : "Brooklyn",
    "cuisine" : "BBQ",
    "grades" : [
        {
            "date" : ISODATE("2015-11-05T00:00:00Z"),
            "grade" : "C",
            "score" : 15
        }
    ],
    "name" : "Big Tex",
    "restaurant_id" : "61704627"
    }
)

18. db.restaurants.update(
{
    "_id" : ObjectId("5e9ca93998816875e84c47ca")
},
{
    $set: { "cuisine" : "Steak and Sea Food" }, $currentDate: { }
})
19. db.restaurants.remove( { "name": "White Castle" } )
```


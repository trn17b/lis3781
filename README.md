# LIS3781 - Advanced Database Management

## Troy Noble

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Use SQL through the command line to create a new DB
    - Create and populate tables
    - Forward engineer tables to CCI server
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Log into **Oracle** Server using RemoteLabs
    - Create and populate **Oracle **tables
    - Upload screenshots of the SQL code
    - Upload screenshots of the populated tables in the Oracle Server
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Log into microsoft sql server
    - Use MS SQL server to create and populate database
    - Use MS SQL server to create an ERD of database
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Use MS SQL server to create and populate more complex database
    - Create an ERD of database using SQL Server
    - Upload screenshot of ERD
6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create database for local municipality
    - Forward engineer tables and values to CCI server
    - Upload screenshots of completed ERD
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Show mongoDB functionality
    - Show JSON queries
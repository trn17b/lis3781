# LIS3781 - Advanced Database Management

## Troy Noble

### Assignment #4 Requirements:

1. Use MS SQL Server to create and populate database
2. Create an ERD using MS SQL server

#### README.md file should include the following items:

* Screenshot of completed A4 ERD



#### Assignment Screenshots:

*A4 ERD*

![ERD](img/a4_erd.png "A4 ERD")



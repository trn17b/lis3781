# LIS3781 - Advanced Database Management

## Troy Noble

### Assignment #1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket

2. AMPPS Installation

3. Questions

4. Entity Relationship Diagram, and SQL Code (optional)

5. Bitbucket repo links:

   a) this assignment and

   b) the completed tutorial (bitbucketstationlocations)

#### A1 Database Business Rules:

The human resource (HR) department of the ACME company wants to contract a database modeler/designer to collect the following employee data for tax purposes: job description, length of employment, benefits, number of dependents and their relationships, DOB of both the employee and any respective dependents. In addition, employees’ histories must be tracked. Also, include the following business rules:

​	• Each employee may have one or more dependents. 

​	• Each employee has only one job. 

​	• Each job can be held by many employees. 

​	• Many employees may receive many benefits.

​	• Many benefits may be selected by many employees (though, while they may not select any benefits— any dependents of employees may be on an employee’s plan). 

Notes: 

• Employee/Dependent tables must use suitable attributes (See Assignment Guidelines);

 In Addition: 

​	• Employee: SSN, DOB, start/end dates, salary; 

​	• Dependent: same information as their associated employee (though, not start/end dates), date added (as dependent), type of relationship: e.g., father, mother, etc. 

​	• Job: title (e.g., secretary, service tech., manager, cashier, janitor, IT, etc.) 

​	• Benefit: name (e.g., medical, dental, long-term disability, 401k, term life insurance, etc.) 

​	• Plan: type (single, spouse, family), cost, election date (plans must be unique) 

​	• Employee history: jobs, salaries, and benefit changes, as well as who made the change and why; 

​	• Zero Filled data: SSN, zip codes (not phone numbers: US area codes not below 201, NJ); 

​	• *All* tables must include notes attribute. 

#### README.md file should include the following items:

* Screenshot of A1 ERD
* Ex1. SQL Solution
* Git commands w/short descriptions



#### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git clone - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of A1 ERD*:

![A1 Diagram](img/A1_erd.png "ERD For A1")

*Screenshot of A1 Example SQL:*

![A1 Ex](img/A1_ex1.png "Example SQL")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")



#### Questions

1. A transaction is a (C)**logical** unit of work that must be either entirely completed or aborted.

2. A consistent database is (B)**One in which all data integrity constraints are satisfied**
3. (B)**Atomicity** requires that all operations of a transaction be completed.
4. (C)**Isolation** means that the data used during the execution of a transaction cannot be used by a second transaction until the first one is completed.
5. All transactions must display (D)**atomicity, durability, consistency, and isolation**
6. A single-user database system automatically ensures (C)**serializability and isolation** of the database, because only one transaction is executed at a time.
7. The ANSI has defined standards that govern SQL database transactions. Transaction support is provided by two SQL statements: (D)**COMMIT** and ROLLBACK.
8. ANSI defines four events that signal the end of a transaction. Of the following events, which is defined by ANSI as being equivalent to a COMMIT?(B)**The end of a program is successfully reached**
9. ANSI defines four events that signal the end of a transaction. Of the following events, which is defined by ANSI as being equivalent to a ROLLBACK?(C)**The program is abnormally terminated**
10. The implicit beginning of a transaction is (C)**When the first SQL statement is encountered**
11. The information stored in the (D)**transaction log** is used by the DBMS for a recovery requirement triggered by a ROLLBACK statement, a program's abnormal termination, or a system failure such as a network discrepancy or a disk crash.
12. One of the three most common data integrity and consistency problems is (A)**lost updates**
13. As long as two transactions, T1 and T2, access (C)**unrelated** data, there is no conflict, and the order of execution is irrelevant to the final outcome.
14. A (A)**database-level** lock prevents the use of any tables in the database from one transaction while another transaction is being processed.
15. (A)**Locks** are required to prevent another transaction from reading inconsistent data.
16. The (C)**lock** manager is responsible for assigning and policing the locks used by the transactions.
17. Lock (A)**granularity** indicated the level of lock use.
18. A (B)**table-level** lock locks the entire table preventing access to any row by a transaction while another transaction is using the table.
19. A (C)**page-level** lock locks the entire diskpage.
20. A (D)**row-level** lock allows concurrent transactions to access different rows of the same table.


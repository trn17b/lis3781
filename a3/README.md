# LIS3781 - Advanced Database Management

## Troy Noble

### Assignment #3 Requirements:

1. Successfully log into the **Oracle** Server using remote labs
2. Create and populate **Oracle **tables

#### README.md file should include the following items:

* Screenshot of **your** SQL code;
* Screenshot of **your** populated tables (w/in the **Oracle environment**);
* Bitbucket repo links

#### Assignment Screenshots:

*Screenshot of SQL Code part 1*:

![Code part 1](img/code_p1.png "My first part of the SQL Code")

*Screenshot of SQL code part 2:*

![Code part 2](img/code_p2.png "Second part of the SQL code")

*Screenshot of populated SQL tables on the Oracle Server*:

![Populated tables](img/populatedTables.png "Populated SQL tables")


